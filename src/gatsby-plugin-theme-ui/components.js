import React from "react"
import ProjectCard from '../@lekoarts/gatsby-theme-cara/components/project-card'
import AboutCard from '../@lekoarts/gatsby-theme-cara/components/about-card'

export default {
  // eslint-disable-next-line react/display-name
  ProjectCard: ({ link, title, bg, children, firstname, surname, vrnt }) => (
    <ProjectCard link={link} title={title} bg={bg} firstname={firstname} surname={surname} vrnt={vrnt}>
      {children}
    </ProjectCard>
  ),
    AboutCard: ({ image, name, title, description, children}) => (
    <AboutCard image={image} name={name} title={title} description={description}>
      {children}
    </AboutCard>
  ),
}
