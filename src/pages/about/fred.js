/** @jsx jsx */
import { jsx } from "theme-ui"

import { Link } from "gatsby"
import styled from "@emotion/styled"

import SVG from "@lekoarts/gatsby-theme-cara/src/components/svg"
import Layout from "../../@lekoarts/gatsby-theme-cara/components/layout"
import Divider from "@lekoarts/gatsby-theme-cara/src/elements/divider"
import Inner from "@lekoarts/gatsby-theme-cara/src/elements/inner"
import Content from "@lekoarts/gatsby-theme-cara/src/elements/content"

import { UpDown, UpDownWide, waveAnimation } from "@lekoarts/gatsby-theme-cara/src/styles/animations"

import { Parallax } from "react-spring/renderprops-addons.cjs"
import ContactMDX from "../../@lekoarts/gatsby-theme-cara/sections/contact.mdx"

import AboutMDX from "../../@lekoarts/gatsby-theme-cara/sections/fred.mdx"

const InnerWave = styled.div`
  path {
    ${waveAnimation(`20s`)};
  }
`

const AboutPage = () => (
    <Layout>
    	<Parallax pages={1.5}>  
    	    <Content sx={{ variant: "texts.about" }} speed={0.4} offset={0.9}>
    		  <Inner>
    		    <ContactMDX />
    		  </Inner>
    		</Content>
    		<Divider fill="divider" speed={0.2} offset={0.65}>
    		    <div sx={{ position: `absolute`, bottom: 0, width: `full`, transform: `matrix(1, 0, 0, -1, 0, 0)` }}>
    		    <InnerWave sx={{ position: `relative`, height: `full`, svg: { width: `100%`, height: `80vh` } }}>
    		      <svg 
    		      	xmlns="http://www.w3.org/2000/svg" 
    		      	id="contact-wave" 
    		      	viewBox="0 0 800 338.05" 
    		      	preserveAspectRatio="none" 
    		      >
    		        <path>
    		          <animate
    		            attributeName="d"
    		            values="M 0 100 Q 250 50 400 200 Q 550 350 800 300 L 800 0 L 0 0 L 0 100 Z;M 0 100 Q 200 150 400 200 Q 600 250 800 300 L 800 0 L 0 0 L 0 100 Z;M 0 100 Q 150 350 400 200 Q 650 50 800 300 L 800 0 L 0 0 L 0 100 Z"
    		            repeatCount="indefinite"
    		            dur="30s"
    		          />
    		        </path>
    		      </svg>
    		    </InnerWave>
    		    </div>
			</Divider>
    		<Divider fill="divider" speed={0.4} offset={0}>
			  	<UpDown>
					<SVG icon="upDown" width={8} color="icon_green" left="40%" top="80%" />
					<SVG icon="box" width={6} color="icon_orange" left="9%" top="80%" />
				</UpDown>
				<UpDownWide>
					<SVG icon="circle" width={6} color="icon_brightest" left="25%" top="95%" />
				</UpDownWide>
			</Divider>
    		<Content sx={{ variant: "texts.black" }} speed={0.4} offset={0}>
    		  	<Inner>
    		    	<div sx={{ px: 48, py: 0 }} >
    					<div sx={{ py: 0, px: [0,32,32], pb: 64, mt: `-72px`, }} >
        			      	<Link to="/">
        			  			<SVG icon="mainlogoBlack" width={170} staticpos color="icon_darkest" />
        					</Link>
        				</div>
        			</div>
                    <AboutMDX />
    		  	</Inner>
    		</Content>
		</Parallax>
    </Layout>
)

export default AboutPage
