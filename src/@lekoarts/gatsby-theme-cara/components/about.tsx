/** @jsx jsx */
import { jsx } from "theme-ui"
import Divider from "@lekoarts/gatsby-theme-cara/src/elements/divider"
import Inner from "@lekoarts/gatsby-theme-cara/src/elements/inner"
import Content from "@lekoarts/gatsby-theme-cara/src/elements/content"
import SVG from "@lekoarts/gatsby-theme-cara/src/components/svg"
import { UpDown, UpDownWide } from "@lekoarts/gatsby-theme-cara/src/styles/animations"
import AboutMDX from "../sections/about.mdx"

const About = ({ offset }: { offset: number }) => (
  <div>
    <Divider bg="#141821" clipPath="polygon(0 15%, 100% 25%, 100% 85%, 0 75%)" speed={0.2} offset={offset} factor={1.5}/>
    <Divider speed={0.1} offset={offset}>
      <UpDown>
        <SVG icon="circle" hiddenMobile width={6} color="icon_brightest" left="4%" top="40%" />
      </UpDown>
      <UpDownWide>
        <SVG icon="triangle" width={12} stroke color="icon_brightest" left="95%" top="80%" />
        <SVG icon="circle" hiddenMobile width={6} color="icon_brightest" left="85%" top="25%" />
      </UpDownWide>
      <SVG icon="box" width={6} color="icon_orange" left="10%" top="30%" />
      <SVG icon="box" width={12} color="icon_orange" left="20%" top="50%" />
    </Divider>
    <Content sx={{ variant:`texts.about` }}speed={0.4} offset={offset+0.21}>
      <Inner>
        <AboutMDX />
      </Inner>
    </Content>
  </div>
)

export default About
