/** @jsx jsx */
import React from "react"
import { Link } from "gatsby"
import { jsx } from "theme-ui"
import Img from "gatsby-image"

type AboutCardProps = {
  image: string
  name: string
  title: string
  description: string
  children: React.ReactNode
}

const AboutCard = ({ image, name, title, description, children }: AboutCardProps) => {

  return (
    
    <div
        sx={{
          variant: "layout.aboutCard",
          display: `grid`,
          gridGap: [3, 3, 3, 4],
          gridTemplateColumns: [`1fr`, `1fr`, `repeat(2, 1fr)`],
          px: 40,
          py: 0,
      }}
    >
      <div>
        <img src={image} alt={name}
         width="100%" />
      </div>
      <div>
        <h2>{name}</h2>
        <h3>{title}</h3>
        <p>{description}</p>
          <Link to="/"><b>&lt; Back to Homepage</b></Link>
      </div>
    </div>
    )
}

export default AboutCard
