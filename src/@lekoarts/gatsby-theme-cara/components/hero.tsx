/** @jsx jsx */
import { jsx } from "theme-ui"
import Divider from "@lekoarts/gatsby-theme-cara/src/elements/divider"
import Inner from "@lekoarts/gatsby-theme-cara/src/elements/inner"
import Content from "@lekoarts/gatsby-theme-cara/src/elements/content"
import SVG from "@lekoarts/gatsby-theme-cara/src/components/svg"
import { UpDown, UpDownWide } from "@lekoarts/gatsby-theme-cara/src/styles/animations"
import Intro from "../sections/intro.mdx"

const Hero = ({ offset }: { offset: number }) => (
  <div>
    <Divider speed={0.2} offset={offset}>
      <SVG icon="brain" hiddenMobile width={350} color="heading" left="65%" top="57%" />
      <UpDown>
      </UpDown>
      <UpDownWide>
        <SVG icon="circlefill" width={12} color="icon_brightest" left="60%" top="5%" />
      </UpDownWide>
      <SVG icon="circle" hiddenMobile width={40} color="icon_brightest" left="-6%" top="70%" />
      <SVG icon="circle" width={12} color="icon_brightest" left="40%" top="80%" />

      <SVG icon="circlefill" width={12} color="icon_brightest" left="20%" top="85%" />
      <SVG icon="circle" width={64} color="icon_brightest" left="87%" top="5%" />
    </Divider>
    <Content sx={{ variant: `texts.bigger`}} speed={0.4} offset={offset}>
      <Inner>
        <SVG icon="mainlogo" width={193} staticpos />
        <br/>
        <Intro />
      </Inner>
    </Content>
  </div>
)

export default Hero
