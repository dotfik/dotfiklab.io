/** @jsx jsx */
import { jsx } from "theme-ui"
import Divider from "@lekoarts/gatsby-theme-cara/src/elements/divider"
import Inner from "@lekoarts/gatsby-theme-cara/src/elements/inner"
import Content from "@lekoarts/gatsby-theme-cara/src/elements/content"
import ProjectsMDX from "../sections/projects.mdx"

const Projects = ({ offset }: { offset: number }) => (
  <div>    
    <Divider bg="divider" clipPath="polygon(0 16%, 100% 4%, 100% 82%, 0 94%)" speed={0.2} offset={offset+1} />
    <Content speed={0.4} offset={offset+0.1} factor={2}>
      <Inner>
        <div sx={{ height: [512,0,0] }}></div>
        <div
          sx={{
            display: `grid`,
            gridGap: [3, 3, 3, 4],
            gridTemplateColumns: [`1fr`, `1fr`, `repeat(2, 1fr)`],
            h2: { gridColumn: `-1/1`, color: `black` },
          }}
        >
          <ProjectsMDX />
        </div>
      </Inner>
    </Content>
  </div>
)

export default Projects
