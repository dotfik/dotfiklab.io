/** @jsx jsx */
import React from "react"
import { useStaticQuery, graphql, Link } from "gatsby"
import { jsx } from "theme-ui"
import Img from "gatsby-image"


type ProjectCardProps = {
  link: string
  title: string
  firstname: surname
  surname: string
  children: React.ReactNode
  bg: string
  vrnt: string
}

const ProjectCard = ({ link, title, firstname, surname, children, bg, vrnt }: ProjectCardProps) => (
      <Link
        to={link}
        rel="noreferrer noopener"
        sx={{
          variant: "texts.black",
          width: `100%`,
          boxShadow: `lg`,
          position: `relative`,
          textDecoration: `none`,
          borderRadius: `lg`,
          px: 4,
          pt: [4, 5],
          pb: [5, 6],
          color: `white`,
          background: `url(`+ bg + `) no-repeat` || `none`,
          backgroundSize: `cover`,
          transition: `all 0.4s cubic-bezier(0.175, 0.885, 0.32, 1.275) !important`,
          "&:hover": {
            color: `white !important`,
            transform: `translateY(-5px)`,
            boxShadow: `xl`,
          },
        }}
      >

        <div sx={{ opacity: 0.75, textShadow: `0 2px 10px rgba(0, 0, 0, 0.3)`, pt:0 }}>{children}</div>
        <div
          sx={{
            variant: vrnt, 
            position: `absolute`,
            textTransform: `uppercase`,
            letterSpacing: `wide`,
            pt: 0,
            fontSize: [4, 5],
            fontWeight: 700,
            lineHeight: 1,
            h4: {
              fontSize: 1,
              fontWeight: `normal`,
              textTransform: `capitalize`,
              color: `white`,
              my: 0,
            },
          }}
        >
          <h4>{title}</h4>
          {firstname}<br/>
          {surname}<br/>
        </div>
        <br/>
      </Link>
    )

export default ProjectCard
